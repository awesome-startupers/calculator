package mkn.pi.calculator.eval

import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.DynamicTest
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.TestFactory
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import java.math.BigDecimal
import java.util.stream.Stream

@SpringBootTest
class EvaluatorComponentTest {
    @Autowired
    private lateinit var evaluatorComponent: EvaluatorComponent

    @TestFactory
    fun evaluateTest(): Stream<DynamicTest> {
        val tests = listOf<Pair<BigDecimal, String>>(
            BigDecimal(0) to "5-5",
            BigDecimal(0) to "5 - 5",
            BigDecimal(0.5) to "1/2",
            BigDecimal(0.25) to "1/4",
            BigDecimal(6) to "2+2*2",
            BigDecimal(8) to "(2+2)*2",
            BigDecimal(1) to "(2/2)",
            BigDecimal(32) to "2^5",
            BigDecimal(0.25) to "0.5^2",
            BigDecimal(0.25) to "0,5^2"
        )

        return tests.stream().map {
            (ans, str) -> DynamicTest.dynamicTest(
                "evaluate: $str",
                { assertEquals(ans, evaluatorComponent.evaluate(str)) }
            )
        }
    }

    @Test
    fun evaluateIncorrectTest() {
        assertThrows(EvaluatorComponent.IncorrectExpressionException::class.java)
        { evaluatorComponent.evaluate("5..5") }
    }
}
