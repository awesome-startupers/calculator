package mkn.pi.calculator.component

import mkn.pi.calculator.dto.EvalRequest
import mkn.pi.calculator.dto.EvalResponse
import mkn.pi.calculator.dto.HistoryResponse
import mkn.pi.calculator.entity.EvalEntity
import mkn.pi.calculator.eval.EvaluatorComponent
import mkn.pi.calculator.repository.CalculatorRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Component
import java.math.BigDecimal

@Component
class CalculatorComponent {
    @Autowired
    lateinit var evaluator: EvaluatorComponent

    @Autowired
    lateinit var repository: CalculatorRepository

    fun calculate(request: EvalRequest): EvalResponse {
        val result = evaluator.evaluate(request.expression)
        val resultString = if (result.toPlainString().length > maxLengthForPlainString) {
            result.toEngineeringString()
        } else {
            result.toPlainString()
        }
        repository.save(EvalEntity(request.chatId, request.expression, resultString))
        return EvalResponse("Result: $resultString")
    }

    fun getHistory(chatId: String): HistoryResponse {
        val list = repository.findAll()
            .filter { t -> t.chatId == chatId }
            .map { t -> HistoryResponse.HistoryExpression(t.expression, t.result) }
            .toList()
        return HistoryResponse(list)
    }

    companion object {
        const val maxLengthForPlainString = 20
    }
}
