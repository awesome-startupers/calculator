package mkn.pi.calculator.dto

import java.io.Serializable

class EvalRequest: Serializable {
    val chatId: String = ""
    val expression: String = ""

    companion object {
        const val serialVersionUID = 2L
    }
}
