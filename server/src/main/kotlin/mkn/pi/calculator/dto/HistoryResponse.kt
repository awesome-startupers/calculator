package mkn.pi.calculator.dto

import java.io.Serializable

class HistoryResponse(val result: List<HistoryExpression>) : Serializable {
    data class HistoryExpression(val expression: String, val result: String)
    companion object {
        const val serialVersionUID = 2L
    }
}
