package mkn.pi.calculator.config

import mkn.pi.calculator.eval.EvaluatorComponent
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration

@Configuration
class CalculatorApplicationConfiguration {
    @Bean
    fun evaluator(): EvaluatorComponent {
        return EvaluatorComponent()
    }
}
