package mkn.pi.calculator.repository

import mkn.pi.calculator.entity.EvalEntity
import org.springframework.data.repository.CrudRepository

interface CalculatorRepository: CrudRepository<EvalEntity, Long>
