package mkn.pi.calculator.entity

import java.math.BigDecimal
import javax.persistence.Entity
import javax.persistence.GeneratedValue
import javax.persistence.GenerationType
import javax.persistence.Id
import javax.persistence.SequenceGenerator

@Entity(name = "wallets")
@SequenceGenerator(allocationSize = 1, name = "wallet_seq", sequenceName = "wallet_seq")
data class EvalEntity(
    val chatId: String,
    val expression: String,
    val result: String,
    @Id @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "wallet_seq") val id: Long? = null
)
