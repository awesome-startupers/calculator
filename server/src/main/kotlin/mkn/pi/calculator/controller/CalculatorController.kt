package mkn.pi.calculator.controller

import mkn.pi.calculator.component.CalculatorComponent
import mkn.pi.calculator.dto.EvalRequest
import mkn.pi.calculator.dto.EvalResponse
import mkn.pi.calculator.dto.HistoryResponse
import mkn.pi.calculator.eval.EvaluatorComponent
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController

@RestController
@RequestMapping("calc")
class CalculatorController(
    @Autowired
    val component: CalculatorComponent
) {

    @GetMapping("hello")
    @Suppress("FunctionOnlyReturningConstant")
    fun getGreeting() = "Hello, World!"

    // Evaluate expression and save it with chatId
    @PostMapping("eval")
    fun postCalc(@RequestBody message: EvalRequest): ResponseEntity<EvalResponse> {
        return try {
            val evalResponse = component.calculate(message)
            ResponseEntity(evalResponse, HttpStatus.OK)
        } catch (_: EvaluatorComponent.IncorrectExpressionException) {
            ResponseEntity(HttpStatus.UNPROCESSABLE_ENTITY)
        }
    }

    // Return all previous expressions by chatId
    @GetMapping("history/{chatId}")
    fun getHistory(@PathVariable("chatId") chatId: String): HistoryResponse {
        return component.getHistory(chatId)
    }
}
