package mkn.pi.calculator.eval

import com.udojava.evalex.Expression
import org.springframework.stereotype.Component
import java.math.BigDecimal

// Class for evaluating math expressions
@Component
class EvaluatorComponent {
    @Suppress("TooGenericExceptionCaught")
    fun evaluate(string: String): BigDecimal {
        try {
            val expr = string.replace(",", ".")
            val expression = Expression(expr)
            return expression.eval()
        } catch (e: RuntimeException) {
            throw IncorrectExpressionException(e)
        }
    }

    class IncorrectExpressionException(cause: Throwable): Exception(cause)
}
