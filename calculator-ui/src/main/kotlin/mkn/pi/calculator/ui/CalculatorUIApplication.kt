package mkn.pi.calculator.ui

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication

@SpringBootApplication
class CalculatorUIApplication

@Suppress("SpreadOperator")
fun main(args: Array<String>) {
	runApplication<CalculatorUIApplication>(*args)
}
