package mkn.pi.calculator.ui.client

import mkn.pi.calculator.ui.dto.EvalRequest
import mkn.pi.calculator.ui.dto.EvalResponse
import mkn.pi.calculator.ui.dto.HistoryResponse
import org.springframework.beans.factory.annotation.Value
import org.springframework.http.HttpEntity
import org.springframework.http.HttpStatus
import org.springframework.stereotype.Service
import org.springframework.web.client.HttpClientErrorException.UnprocessableEntity
import org.springframework.web.client.RestTemplate

@Service
class Client(
    @param:Value("\${server.path}") private val serverPath: String
) {
    private val _client = RestTemplate()


    fun getHistory(chatId: String): List<HistoryResponse.HistoryExpression>? {
        val path = "$serverPath/history/{chatId}"

        val response = _client.getForEntity(path, HistoryResponse::class.java, mapOf("chatId" to chatId))
        if (response.statusCode != HttpStatus.OK) {
            return null
        }

        return response.body?.result
    }

    fun eval(chatId: String, expression: String): String {
        val path = "$serverPath/eval"

        val request: HttpEntity<EvalRequest> = HttpEntity(EvalRequest(chatId, expression))
        return try {
            val response: EvalResponse? = _client.postForObject(path, request, EvalResponse::class.java)
            if (response == null) {
                return "Internal error"
            }

            response.result
        } catch (_: UnprocessableEntity) {
            "Error in expression"
        }
    }
}
