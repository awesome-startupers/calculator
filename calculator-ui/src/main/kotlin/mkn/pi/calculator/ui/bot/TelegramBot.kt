package mkn.pi.calculator.ui.bot

import mkn.pi.calculator.ui.client.Client
import mkn.pi.calculator.ui.dto.HistoryResponse
import org.springframework.beans.factory.annotation.Value
import org.springframework.stereotype.Component
import org.telegram.telegrambots.bots.TelegramLongPollingBot
import org.telegram.telegrambots.meta.TelegramBotsApi
import org.telegram.telegrambots.meta.api.methods.send.SendMessage
import org.telegram.telegrambots.meta.api.objects.Message
import org.telegram.telegrambots.meta.api.objects.Update
import org.telegram.telegrambots.meta.api.objects.replykeyboard.InlineKeyboardMarkup
import org.telegram.telegrambots.meta.api.objects.replykeyboard.ReplyKeyboardMarkup
import org.telegram.telegrambots.meta.api.objects.replykeyboard.buttons.InlineKeyboardButton
import org.telegram.telegrambots.meta.api.objects.replykeyboard.buttons.KeyboardRow
import org.telegram.telegrambots.meta.exceptions.TelegramApiException
import java.lang.Long.max


@Component
final class TelegramBot(
    telegramBotsApi: TelegramBotsApi,
    @param:Value("\${telegram-bot.name}") private val botUsername: String,
    @param:Value("\${telegram-bot.token}") private val botToken: String,
    private val client: Client,
) : TelegramLongPollingBot() {
    private var requestMessage: Message = Message()

    init {
        telegramBotsApi.registerBot(this)
    }

    override fun getBotToken(): String {
        return botToken
    }

    override fun getBotUsername(): String {
        return botUsername
    }

    override fun onUpdateReceived(request: Update) {
        val response = SendMessage()

        requestMessage = request.message
        requestMessage.text = requestMessage.text.removePrefix("@startup_calculator_bot ")
        response.setChatId(requestMessage.getChatId().toString())

        if (requestMessage.getText().equals("/history")) {
            historyHandler(response)
        } else if (requestMessage.getText().startsWith("/calc")) {
            val expressions = requestMessage.getText().split(" ", limit = 2)
            if (expressions.size != 2) {
                defaultMsg(response, "Please specify the expression")
            } else {
                expressionHandler(expressions[1], response)
            }
        } else {
            helpHandler(response)
        }
    }

    private fun historyHandler(response: SendMessage) {
        val history = client.getHistory(response.chatId)
        if (history == null) {
            defaultMsg(response, "internal error")
        } else {
            inlineMsg(response, history, "History:")
        }
    }

    private fun expressionHandler(expression: String, response: SendMessage) {
        defaultMsg(response, client.eval(response.chatId, expression))
    }

    private fun helpHandler(response: SendMessage) {
        defaultMsg(response, """
            If you want to calculate the result of an expression, type: /calc <expression>
            If you want to get history, type: /history
        """.trimIndent())
    }

    @Throws(TelegramApiException::class)
    private fun defaultMsg(response: SendMessage, msg: String) {
        response.text = msg
        execute(response)
    }

    private fun inlineMsg(response: SendMessage, history: List<HistoryResponse.HistoryExpression>, msg: String) {
        val keyboardMarkup = InlineKeyboardMarkup()

        var message = msg

        val rowList: MutableList<List<InlineKeyboardButton>> = mutableListOf()

        val nonClickableSize = max(history.size - maxNumberOfClickableHistory, 0L)

        history.stream().limit(nonClickableSize).forEach {
            message += "\n${it.expression}=${it.result}"
        }

        history.stream().skip(nonClickableSize).forEach{
            val button = InlineKeyboardButton()
            button.text = "${it.expression}=${it.result}"
            button.switchInlineQueryCurrentChat = "/calc ${it.expression}"
            rowList.add(listOf(button))
        }

        keyboardMarkup.keyboard = rowList

        response.replyMarkup = keyboardMarkup
        defaultMsg(response, message)
    }

    @Suppress("UnusedPrivateMember")
    private fun keyboardMsg(response: SendMessage, texts: List<String>, msg: String) {
        val keyboardMarkup = ReplyKeyboardMarkup()

        val rowList: MutableList<KeyboardRow> = mutableListOf()

        texts.forEach{
            rowList.add(KeyboardRow().apply { this.add(it) })
        }

        keyboardMarkup.setOneTimeKeyboard(true)
        keyboardMarkup.keyboard = rowList

        response.setReplyMarkup(keyboardMarkup)
        defaultMsg(response, msg)
    }

    companion object {
        private const val maxNumberOfClickableHistory = 10L
    }
}
