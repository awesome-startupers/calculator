package mkn.pi.calculator.ui.config

import com.fasterxml.jackson.databind.ObjectMapper
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.telegram.telegrambots.meta.TelegramBotsApi;
import org.telegram.telegrambots.meta.exceptions.TelegramApiException;
import org.telegram.telegrambots.updatesreceivers.DefaultBotSession;

@Configuration
class AppConfig {
    @Bean
    @Throws(TelegramApiException::class)
    fun telegramBotsApi(): TelegramBotsApi? {
        return TelegramBotsApi(DefaultBotSession::class.java)
    }
}
