package mkn.pi.calculator.ui.dto

import java.io.Serializable

class EvalResponse(val result: String) : Serializable {
    companion object {
        const val serialVersionUID = 1L
    }
}
