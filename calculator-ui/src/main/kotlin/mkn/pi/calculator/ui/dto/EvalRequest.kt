package mkn.pi.calculator.ui.dto

import java.io.Serializable

class EvalRequest(val chatId: String, val expression: String): Serializable {

    companion object {
        const val serialVersionUID = 2L
    }
}
